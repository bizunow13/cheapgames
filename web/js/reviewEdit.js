$(document).ready(function() {

	/*
	 * Insert image tag
	 */

	jQuery.fn.extend({
		insertAtCaret: function(myValue){
			return this.each(function(i) {
				if (document.selection) {
					//For browsers like Internet Explorer
					this.focus();
					sel = document.selection.createRange();
					sel.text = myValue;
					this.focus();
				} else if (this.selectionStart || this.selectionStart == '0') {
					//For browsers like Firefox and Webkit based
					var startPos = this.selectionStart;
					var endPos = this.selectionEnd;
					var scrollTop = this.scrollTop;
					this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
					this.focus();
					this.selectionStart = startPos + myValue.length;
					this.selectionEnd = startPos + myValue.length;
					this.scrollTop = scrollTop;
				} else {
					this.value += myValue;
					this.focus();
				}
			})
		}
	});

	function insertImage() {
		var img = $(this).data('full-path');
		$('#text').insertAtCaret('[img]' + img + '[/img]');
	}

	$('.image').click(insertImage);

	/*
	 * Change review image
	 */

	$('#review-image').click(function () {
		$('#imgReview').click();
	});

	$('#imgReview').change(function () {
		$('#uploadImgReview').submit();
	});

	$('#uploadImgReview').submit(function () {

		if ($('#imgReview')[0].files[0] == undefined)
			return false;

		var formData = new FormData();
		formData.append('file', $('#imgReview')[0].files[0]);
		formData.append('type', 'game-logo');

		var success = function (data) {
			if (data['result']) {
				$.post('?r=reviews/ajax_change_review_image', {
					game_id: $('#reviewId').val(),
					image: data['file']['path']
				}, function(result) {
					if (result['result']) {
						$('#review-image').attr('src', data['file']['path']);
					} else {
						alert(result['error']);
					}
				}, 'JSON');
			} else {
				alert(data['error']);
			}
		};

		var error = function (data) {
			alert(data['error']);
		};

		$.ajax({
			type: 'POST',
			url: '?r=upload/ajax_upload_file',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "json",
			success: success,
			error: error
		});

		return false;
	});

	/*
	 * Add new image
	 */

	$('.add-image').click(function () {
		$('#imgPost').click();
	});

	$('#imgPost').change(function () {
		$('#uploadImgPost').submit();
	});

	$('#uploadImgPost').submit(function () {
		if ($('#imgPost')[0].files[0] == undefined)
			return false;

		var formData = new FormData();
		formData.append('file', $('#imgPost')[0].files[0]);
		formData.append('type', 'post-image');

		var success = function (data) {
			if (data['result']) {
				var img = $('<img>', {
					'class': 'image',
					'data-full-path': data['file']['path'],
					'src': data['file']['path']
				});

				img.bind('click', insertImage);

				$('.attach-images').prepend(img);
			} else {
				alert(data['error']);
			}
		};

		var error = function (data) {
			alert(data['error']);
		};

		$.ajax({
			type: 'POST',
			url: '?r=upload/ajax_upload_file',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "json",
			success: success,
			error: error
		});

		return false;
	});

	/*
	 * Save
	 */

	function saveButtonDefault() {
		var button = $('#saveButton');
		button.attr('disabled', false);
		button.text('Save');
	}

	function displayStatus(text) {
		$('#saveButton').text(text);
		setTimeout(saveButtonDefault, 1000);
	}

	$('#saveButton').click(function () {
		$(this).attr('disabled', true);

		var id = $('#reviewId').val();
		var public = $('#status').val();
		var title = $('#title').val();
		var descr = $('#descr').val();
		var text = $('#text').val();

		var images = [];
		$('.image').each(function () {
			images.push($(this).data('full-path'));
		});

		$.post('?r=reviews/ajax_save_review', {
			id: id,
			public: public,
			title: title,
			descr: descr,
			text: text,
			images: images
		}, function(result) {
			if (result['saved']) {
				displayStatus('Success');
			} else {
				displayStatus('Error');
			}
		}, 'JSON');
	});
	
	/*
	 * Check length
	 */
	
	$('.max-length').keyup(function () {
		var text = $(this).val();
		var mess = $($(this).data('message'));
		var maxLength = $(this).data('max-length');
		if (text.length > maxLength) {
			$('#saveButton').attr('disabled', true);
			mess.show();
		} else {
			$('#saveButton').attr('disabled', false);
			mess.hide();
		}
	});

	/*
	 * Edit tools
	 */

	function wrapText(elementID, openTag, closeTag) {
		var textArea = elementID;
		var len = textArea.val().length;
		var start = textArea[0].selectionStart;
		var end = textArea[0].selectionEnd;
		var selectedText = textArea.val().substring(start, end);
		var replacement = openTag + selectedText + closeTag;
		textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len));
	}

	$('.edit-tool').click(function () {
		var openTag = $(this).data('open-tag');
		var closeTag = $(this).data('close-tag');
		wrapText($('#text'), openTag, closeTag);
	});

});
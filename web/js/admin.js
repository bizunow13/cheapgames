$(document).ready(function() {

	/*
	 * Page choose
	 */

	$('.adminSectionChoose').click(function () {
        $('.adminSectionChoose').removeClass('active');
        $(this).addClass('active');
        $('.adminSection').hide();
        $($(this).data('enable')).show();
	});

	/*
	 * Top and New Games
	 */

    var gameTr = null;

    $('.changeGameImage').click(function() {
        $('#imgFile').click();
        gameTr = $(this).closest('tr');
    });

	// Вот это нужно переписать. Не получится так установить две одинаковых картинки подряд
	// Т.к. не будет события change
	$('#imgFile').change(function () {
        $('#uploadImg').submit();
    });

    $('#uploadImg').submit(function () {

        if ($('#imgFile')[0].files[0] == undefined)
			return false;

		var formData = new FormData();
		formData.append('file', $('#imgFile')[0].files[0]);
		formData.append('type', 'game-logo');

		var success = function (data) {
			$.post('?r=admin/ajax_change_image', {
				game_id: gameTr.data('game-id'),
				image: data['file']['path']
			}, function(result) {
				if (result['result']) {
					gameTr.find('.img-game').attr('src', data['file']['path']);
				} else {
					alert(result['error']);
				}
			}, 'JSON');
		};

		var error = function (data) {
			alert(data['error']);
		};

		$.ajax({ // Может можно просто заюзать this.post?
			type: 'POST',
			url: '?r=upload/ajax_upload_file',
			data: formData,
			processData: false, // Что это?
			contentType: false, // Что это?
			dataType: "json",
			success: success,
			error: error
		});

		return false;
    });

	$('.game-option').change(function () {
		var game_id = $(this).closest('tr').data('game-id');
		var game_option = $(this).data('game-option');
		var value = $(this).val();
		var _this = $(this);

		_this.prop('disabled', true);

		$.post('?r=admin/ajax_game_option_change', {
			game_id: game_id,
			game_option: game_option,
			value: value
		}, function(result) {
			_this.prop('disabled', false);
			if (!result['saved']) {
				alert("Error while saving!");
			}
		}, 'JSON');
	});

	/*
	 * Import.IO
	 */

	$('.settings-option').change(function() {
		var game_id = $(this).data('store-id');
		var store_option = $(this).data('store-option');
		var value = $(this).val();
		var _this = $(this);

		_this.prop('disabled', true);

		$.post('?r=admin/ajax_store_option_change', {
			store_id: store_id,
			store_option: store_option,
			value: value
		}, function(result) {
			_this.prop('disabled', false);
			if (!result['saved']) {
				alert("Error while saving!");
			}
		}, 'JSON');
	});

	/*
	 * STORE OPTIONS
	 */

	function store_option_change() {
		var store_id = $(this).data('store-id');
		var store_option = $(this).data('store-option');
		var value = $(this).val();
		var _this = $(this);

		_this.prop('disabled', true);

		$.post('?r=admin/ajax_store_option_change', {
			store_id: store_id,
			store_option: store_option,
			value: value
		}, function(result) {
			_this.prop('disabled', false);
			if (!result['saved']) {
				alert("Error while saving!");
			}
		}, 'JSON');
	};

	$('.store-option').change(store_option_change);

	function store_delete() {
		var store_id = $(this).data('store-id');
		var row = $(this).closest('tr');
		var _this = $(this);

		row.find('.store-name').prop('disabled', true);
		row.find('.store-connector').prop('disabled', true);
		_this.prop('disabled', true);

		$.post('?r=admin/ajax_store_delete', {
			store_id: store_id
		}, function(result) {
			if (!result['deleted']) {
				row.find('.store-name').prop('disabled', false);
				row.find('.store-connector').prop('disabled', false);
				_this.prop('disabled', false);
				alert("Error while deleting!");
			} else {
				row.remove();
			}
		}, 'JSON');
	}

	$('.store-delete').click(store_delete);
	
	var new_row_html = '<tr>\
	<td width="20%"> <input type="text" class="form-control store-option" data-store-option="name" data-store-id="" value=""> </td>\
    <td width="55%"> <input type="text" class="form-control store-option" data-store-option="io_connector" data-store-id="" value=""> </td>\
    <td width="20%"> <input type="text" class="form-control store-option" data-store-option="partner_key" data-store-id="" value=""> </td>\
    <td width="5%">  <button class="btn btn-default store-delete" data-store-id=""> <span class="glyphicon glyphicon-trash"></span> </button> </td>\
    </tr>';

	$('.store-add').click(function() {
		var input_store_name = $('#new-store-name');
		var input_store_connector = $('#new-store-connector');
		var input_store_partner_key = $('#new-store-partner-key');

		var store_name = input_store_name.val();
		var store_connector = input_store_connector.val();
		var store_partner_key = input_store_partner_key.val();

		if (store_name != '' && store_connector != '') {
			input_store_name.prop('disabled', true);
			input_store_connector.prop('disabled', true);
			input_store_partner_key.prop('disabled', true);

			$.post('?r=admin/ajax_save_new_store', {
				store_name: store_name,
				store_connector: store_connector,
				store_partner_key: store_partner_key
			}, function(result) {
				input_store_name.prop('disabled', false);
				input_store_connector.prop('disabled', false);
				input_store_partner_key.prop('disabled', false);
				if (!result['saved']) {
					alert("Error while saving!");
				} else {
					var new_row = $(new_row_html);
					var new_row_store_name_input = new_row.find('input[data-store-option="name"]');
					var new_row_store_connector_input = new_row.find('input[data-store-option="io_connector"]');
					var new_row_store_partner_key_input = new_row.find('input[data-store-option="partner_key"]');
					var new_row_delete_button = new_row.find('.store-delete');

					new_row_store_name_input.val(store_name);
					new_row_store_connector_input.val(store_connector);
					new_row_store_partner_key_input.val(store_partner_key);

					new_row.find('.store-option').attr('data-store-id', result['new_store_id']);
					new_row.find('.store-option').bind('change', store_option_change);

					new_row_delete_button.attr('data-store-id', result['new_store_id']);
					new_row_delete_button.bind('click', store_delete);
					
					input_store_name.val('');
					input_store_connector.val('');
					input_store_partner_key.val('');

					$("#store-table tr:last").before(new_row);
					//alert("Value successful change!");
				}
			}, 'JSON');
		}
	});
});
$(document).ready(function() {
	
	$('.sort-option').click(function () {
		$('.sort-option').css('text-decoration', 'none');
		$('.results').hide();
		$($(this).data('toggle')).show();
		$(this).css('text-decoration', 'underline');
		return false;
	});

	/*
	 * GAME URL CLICK
	 */

	$('.game-url').click(function() {
		var text = $('#search-request').val();

		var tr = $(this).closest('tr');
		var game = tr.data('game');
		var platform = tr.data('platform');
		var store = tr.data('store');
		var price = tr.data('price');

		$.post('?r=site/ajax_save_follow', {
			text: text,
			game: game,
			platform: platform,
			store: store,
			price: price
		}, function(result) {
			//console.log(result);
		}, 'JSON');
	});

	/*
	 * TOP & NEW GAMES
	 */

	function submit(request) {
		var form = $('.search-form');
		var searchInput = $('#search-request');
		searchInput.val(request);
		form.submit();
	}

	$('.new-game').click(function() {
		submit($(this).data('request'));
	});

	$('.top-request').click(function() {
		submit($(this).text());
	});

	/*
	 * SUBSCRIBE 
	 */

	function subscribeButtonUpdate() {
		var email = $("#subscribe-email").val();
		var subscribeButton = $('#subscribe-button');
		var re = /^\w+@\w+\.\w{2,4}$/i;
		if (email == '' || !re.test(email) || !$('.selected').length) {
			subscribeButton.prop('disabled', true);
		} else {
			subscribeButton.prop('disabled', false);
		}
	}

	$('.selectable .game-url').click(function(e){
		e.stopPropagation(); 
	});

	$('.selectable').click(function() {
		$(this).toggleClass('selected')
		subscribeButtonUpdate(); 
	});

	$("#subscribe-email").keyup(function() {
		subscribeButtonUpdate();
	});

	$('#subscribe-button').click(function() {
		var email = $("#subscribe-email").val();
		var gameList = [];

		$('.selected').each(function(i) {
			gameList.push({
				'store': $(this).data('store'),
				'url': $(this).data('url'),
				'partnerUrl': $(this).data('partner-url'),
				'name': $(this).data('game'),
				'price': $(this).data('price')
			});
		});

		var subscribeButton = $(this);
		subscribeButton.prop('disabled', true);
		$('.div-subscribe-result').css('display', 'none');

		$.post('?r=subscribe/ajax_subscribe', {
			email: email,
			games: JSON.stringify(gameList)
		}, function(result) {
			subscribeButton.prop('disabled', false);
			if (result['subscribed']) {
				$('#subscribe-success').css('display', 'inline-block');
			} else {
				$('#subscribe-fail').css('display', 'inline-block');
			}
		}, 'JSON');
	});
	
});
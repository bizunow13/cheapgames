<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h1>Цена на отслеживаемые вами товары изменилась!</h1>

<p>
	<?php foreach ($games as $game) { ?>
		<?php 
			$removeLink = Url::to(['subscribe/delete_game', 'token' => $game->remove_token], true);
			$redirectLink = Url::to(['subscribe/redirect_to_game', 'token' => $game->redirect_token], true);
		?>
		<p> <a href=<?="$redirectLink"?>> <?=$game->store.' '.$game->name.' '.$game->price.'р.'?></a> <a href=<?="$removeLink"?>>(не отслеживать)</a> 
	<?php } ?>
</p>

Вы можете <a href="<?=Url::to(['subscribe/unsubscribe', 'token' => $unsubscribeToken], true)?>">отписаться</a> от рассылки.
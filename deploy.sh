# Site on work
echo "Delete old dirs"
#rm -rf public_html
#mv public_html_work public_html
rm -rf .composer
rm -rf assets
rm -rf commands
rm -rf config
rm -rf controllers
rm -rf mail
rm -rf models
rm -rf runtime
rm -rf views

# Download repo
echo "Download repo from git"
git clone https://gitlab.com/bizunow13/cheapgames.git

# Copy files from repo to server root
echo "Copy files from repo to server root"
mv cheapgames/* ~/

# Install vendor via composer
echo "Composer magick"
/opt/php5.6/bin/php composer.phar global require "fxp/composer-asset-plugin":"^1.0" --no-plugins
/opt/php5.6/bin/php composer.phar install

# delete repo copy
echo "Delete temp dir"
rm -rf cheapgames

# Site on hz
echo "Rename public_html to public_html_work"
#mv public_html public_html_work
echo "Rename web to public_html"
#mv public_html web
mv web public_html

rm -rf web

# Permissions
chmod 700 update.sh
chmod 700 automailer.sh
chmod 700 deploy.sh
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
$this->registerCssFile("/css/mainLayout.css?v=1.0");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="Keywords" content="steam, дешево, низкие цены, gta, tomb raider, dark souls, mafia, mafia iii, mafia 3">
    <meta name="description" content="Найди самые дешевые игры среди крупнейших проверенных магазинов рунета!">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <div class="site-index">
            <div class="header-content">
                <div class="title">
                    <a href="<?=Yii::$app->homeUrl?>">
                        <span class="text-white">GAMES</span><div class="space"></div><span class="text-green">CHEAP</span>
                    </a>
                </div>
                <div class="delimiter"></div>
                <div class="description">Поиск игр по наименьшей цене!</div>
            </div>
            <div class="body-content">
                <?=$content?>
            </div>
            <div class="footer-content">
                <div class="date">
                    <?=date('Y')?> &copy; GAMESCHEAP.RU
                </div>
                <div class="contacts">
                    <a href="https://vk.com/gamescheapru">ВКонтакте</a>
                    <a href="mailto:contact@gamescheap.ru">contact@gamescheap.ru</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
$this->registerJsFile('/js/jquery.min.js',  ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/js/search.js?v=1.7',  ['position' => yii\web\View::POS_END]);
$this->registerCssFile("/css/mainPage.css?v=1.0");
$this->title = 'Найди дешевые ключи steam, origin, uplay!';
/*echo '<pre>';
echo var_dump($results);
echo '</pre>';
die();*/

use yii\helpers\Url; ?>

<?php
    if (YII_ENV_PROD) {
        include_once("analytics.php");
    }
?>

<div class="container">
    <form class="search-form" role="form">
        <div class="div-game input-group">
            <input name="game" type="text" id="search-request" placeholder="Например: <?=$example?>" value="<?=$request?>"/>
            <span class="input-group-btn search-button-span">
                <button class="search-button" type="submit">Найти</button>
            </span>
        </div>
        <div class="info">
            <div class="div-top-new">
                <div class="div-top">
                    <h1>Топ запросов</h1>
                    <?php foreach ($topRequests as $index => $request) { ?>
                        <div class="div-top-row">
                            <?=$index + 1?>. <a class="top-request" href="#"><?=$request['text']?></a>
                        </div>
                    <?php } ?>
                </div>
                <div class="div-new">
                    <h1>Рекомендации</h1>
                    <div class="div-new-table">
                        <?php foreach ($newGames as $game) { ?>
                            <div class="div-new-game new-game" data-request="<?=$game->request?>">
                                <img class="img-game" src="<?=$game->img?>">
                                <div class="div-game-caption"><?=$game->name?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php if (Yii::$app->session->hasFlash('unSubscribe')) { ?>
    <div class="container">
        Вы отписались от рассылки!
    </div>
<?php } elseif (Yii::$app->session->hasFlash('deleteGame')) { ?>
    <div class="container">
        Игра удалена из списка отслеживаемых!
    </div>
<?php } ?>
<?php if ($nothingFound === true) { ?>
    <div class="container">
        <h1 class="h1-nothing-found">Ни чего не найдено</h1>
    </div>
<?php } ?>
<!--
ИЗБАВИТСЯ ОТ H1 ВЕЗДЕ.
-->
<?php if (!$nothingFound && $results) { ?>
<div class="container">
    <div class="results-info">
        <div class="count">
            <?=$resultsCount['total'] . ' ' . $resultsCount['count']?>
        </div>
        <div class="sort">
            Сортировать по:
            <a class="sort-option" data-toggle="#results-by-store" href="#">магазину</a>,
            <a class="sort-option" data-toggle="#results-by-game" href="#">игре</a>,
            <a class="sort-option" data-toggle="#results-by-platform" href="#">платформе</a>
        </div>
    </div>
    <!-- GROUP BY STORES -->
    <div class="results" id="results-by-store">
        <?php foreach ($results as $store => $data) { ?>
            <div class="div-results-section">
                <h1><?=$store?></h1>
                <table class="table table-results" id="store-table">
                    <thead>
                        <tr>
                            <th width="75%">Игра</th>
                            <th width="15%">Платформа</th>
                            <th width="10%">Цена</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $game) { ?>
                            <tr class="selectable"
                                data-url="<?=$game['url']?>"
                                data-partner-url="<?=isset($game['partner_url']) ? $game['partner_url'] : ''?>"
                                data-store="<?=$store?>"
                                data-game="<?=$game['name']?>"
                                data-platform="<?=$game['platform']?>"
                                data-price="<?=$game['price']?>"
                            >
                                <td> <a class="game-url" href="<?=isset($game['partner_url']) ? $game['partner_url'] : $game['url']?>" target="_blank"> <?=$game['name']?> </a> </td>
                                <td class="td-platform"> <?= $game['platform'] == "Unknown" ? "Неизвестно" : $game['platform'] ?> </td>
                                <td class="td-price"> <?= $game['price'] != -1 ? $game['price'] . " руб." : "" ?> </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
    <!-- GROUP BY GAME -->
    <div class="results" id="results-by-game" hidden>
        <?php
            $unique = [];
            if (isset($resultsByGame['Unique'])) {
                $unique = $resultsByGame['Unique'];
                unset($resultsByGame['Unique']);
            }
        ?>
        <?php foreach ($resultsByGame as $game => $_stores) { ?>
            <div class="div-results-section">
                <h1><?=$game?></h1>
                <table class="table table-results" id="game-table">
                    <thead>
                        <tr>
                            <th width="75%">Магазин</th>
                            <th width="15%">Платформа</th>
                            <th width="10%">Цена</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($_stores as $store) { ?>
                            <tr class="selectable"
                                data-url="<?=$store['url']?>"
                                data-partner-url="<?=isset($store['partner_url']) ? $store['partner_url'] : '' ?>"
                                data-store="<?=$store['store']?>"
                                data-game="<?=$game?>"
                                data-platform="<?=$store['platform']?>"
                                data-price="<?=$store['price']?>"
                            >
                                <td> <a class="game-url" href="<?=isset($store['partner_url']) ? $store['partner_url'] : $store['url']?>" target="_blank"> <?= $store['store'] ?> </a> </td>
                                <td class="td-platform"> <?= $store['platform'] == "Unknown" ? "Неизвестно" : $store['platform'] ?> </td>
                                <td class="td-price"> <?= $store['price'] != -1 ? $store['price'] . "&nbsp;руб." : "" ?> </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <?php if ($unique) { ?>
            <div class="div-results-section">
                <h1>Прочее</h1>
                <table class="table table-results" id="game-table">
                    <thead>
                        <tr>
                            <th width="60%">Игра</th>
                            <th width="15%">Магазин</th>
                            <th width="15%">Платформа</th>
                            <th width="10%">Цена</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($unique as $game) { ?>
                            <tr class="selectable"
                                data-url="<?=$game['url']?>"
                                data-partner-url="<?=isset($game['partner_url']) ? $game['partner_url'] : '' ?>"
                                data-store="<?=$game['store']?>"
                                data-game="<?=$game['name']?>"
                                data-platform="<?=$game['platform']?>"
                                data-price="<?=$game['price']?>"
                            >
                                <td> <a class="game-url" href="<?=isset($game['partner_url']) ? $game['partner_url'] : $game['url']?>" target="_blank"> <?= $game['name'] ?> </a> </td>
                                <td class="td-store"> <?= $game['store'] ?> </td>
                                <td class="td-platform"> <?= $store['platform'] == "Unknown" ? "Неизвестно" : $store['platform'] ?> </td>
                                <td class="td-price"> <?= $game['price'] != -1 ? $game['price'] . "&nbsp;руб." : "" ?> </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
    <!-- GROUP BY PLATFORM -->
    <div class="results" id="results-by-platform" hidden>
        <?php
            $unknown = [];
            if (isset($resultsByPlatform['Unknown'])) {
                $unknown = $resultsByPlatform['Unknown'];
                unset($resultsByPlatform['Unknown']);
            }
        ?>
        <?php foreach ($resultsByPlatform as $platform => $data) { ?>
            <div class="div-results-section">
                <h1><?=$platform?></h1>
                <table class="table table-results" id="platform-table">
                    <thead>
                        <tr>
                            <th width="75%">Игра</th>
                            <th width="15%">Магазин</th>
                            <th width="10%">Цена</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $game) { ?>
                            <tr class="selectable"
                                data-url="<?=$game['url']?>"
                                data-partner-url="<?=isset($game['partner_url']) ? $game['partner_url'] : '' ?>"
                                data-store="<?=$game['store']?>"
                                data-game="<?=$game['name']?>"
                                data-platform="<?=$platform?>"
                                data-price="<?=$game['price']?>"
                            >
                                <td> <a class="game-url" href="<?=isset($game['partner_url']) ? $game['partner_url'] : $game['url']?>" target="_blank"> <?=$game['name']?> </a> </td>
                                <td class="td-store"> <?= $game['store'] ?> </td>
                                <td class="td-price"> <?= $game['price'] != -1 ? $game['price'] . "&nbsp;руб." : "" ?> </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <?php if ($unknown) { ?>
            <div class="div-results-section">
            <h1>Неизвестно</h1>
                <table class="table table-results" id="game-table">
                    <thead>
                        <tr>
                            <th width="75%">Игра</th>
                            <th width="15%">Магазин</th>
                            <th width="10%">Цена</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($unknown as $game) { ?>
                            <tr class="selectable"
                                data-url="<?=$game['url']?>"
                                data-partner-url="<?=isset($game['partner_url']) ? $game['partner_url'] : '' ?>"
                                data-store="<?=$game['store']?>"
                                data-game="<?=$game['name']?>"
                                data-platform="<?=$game['platform']?>"
                                data-price="<?=$game['price']?>"
                            >
                                <td> <a class="game-url" href="<?=isset($game['partner_url']) ? $game['partner_url'] : $game['url']?>" target="_blank"> <?= $game['name'] ?> </a> </td>
                                <td class="td-store"> <?= $game['store'] ?> </td>
                                <td class="td-price"> <?= $game['price'] != -1 ? $game['price'] . "&nbsp;руб." : "" ?> </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
    <div class="div-subscribe">
        <h2>Отслеживай цены на избранные товары!</h2>
        <div class="div-subscribe-form">
            <input type="email" id="subscribe-email" placeholder="Адрес электронной почты">
            <button id="subscribe-button" disabled="true">Отслеживать!</button>
            <div class="div-subscribe-result" id="subscribe-success" hidden> <span class="glyphicon glyphicon-ok"></span> </div>
            <div class="div-subscribe-result" id="subscribe-fail" hidden> <span class="glyphicon glyphicon-remove"></span> </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="container">
    <div class="reviews">
        <h2>Обзоры</h2>
        <div class="reviews-content">
            <?php foreach ($lastReviews as $review) { ?>
                <div class="review">
                    <div class="review-img">
                        <a href="<?=Url::toRoute(['reviews/show', 'id' => $review->id])?>">
                            <img class="img-preview" src="<?=$review->preview?>">
                        </a>
                    </div>
                    <div class="post">
                        <div class="title">
                            <a href="<?=Url::toRoute(['reviews/show', 'id' => $review->id])?>"><?=$review->title?></a>
                        </div>
                        <div class="descr"><?=$review->descr?></div>
                        <div class="footer"><?=date("d.m.Y H:i", strtotime($review->time))?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
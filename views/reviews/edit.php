<?php
$this->registerJsFile('\js\jquery.min.js',  ['position' => yii\web\View::POS_END]);
$this->registerJsFile('\js\reviewEdit.js?v=1.0',  ['position' => yii\web\View::POS_END]);
$this->registerCssFile('\css\reviewEdit.css?v=1.0');
$this->title = 'Review edit';

use yii\helpers\Url;?>

<div class="site-index">
    <div class="body-content">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
        <input type="hidden" id="reviewId" value="<?=isset($_GET['id']) ? $_GET['id'] : null ?>">
        <form id="uploadImgReview" hidden>
            <input type="file" id="imgReview" />
        </form>
        <form id="uploadImgPost" hidden>
            <input type="file" id="imgPost" />
        </form>
        <div class="container">
            <div class="form-group">
                <div>
                    <label for="image">Image <span class="gray-text">230x108, png, jpg, jpeg</span></label>
                </div>
                <img class="image-choose" id="review-image" src="<?=$review->preview ? $review->preview : 'img/previews/blank.png' ?>">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status">
                    <option <?=$review->public ? "selected" : ""?> value="1">Published</option>
                    <option <?=$review->public ? "" : "selected"?> value="0">Hidden</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Title <span id="long-title" class="warning" hidden>too long!</span></label>
                <input id="title" class="form-control max-length" type="text" data-max-length="45" data-message="#long-title" value="<?=$review->title?>">
            </div>
            <div class="form-group">
                <label for="descr">Description <span id="long-descr" class="warning" hidden>too long!</span></label>
                <textarea id="descr" class="form-control max-length" data-max-length="300" data-message="#long-descr" rows="2"><?=$review->descr?></textarea>
            </div>
            <div class="form-group">
                <label for="text">Text</label>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default edit-tool" data-open-tag="[p]" data-close-tag="[/p]">
                            &sect;
                        </button>
                        <button type="button" class="btn btn-default edit-tool" data-open-tag="[b]" data-close-tag="[/b]">
                            <strong>Bold</strong>
                        </button>
                        <button type="button" class="btn btn-default edit-tool" data-open-tag="[i]" data-close-tag="[/i]">
                            <i>Italic</i>
                        </button>
                        <button type="button" class="btn btn-default edit-tool" data-open-tag="[s]" data-close-tag="[/s]">
                            <s>Strike</s>
                        </button>
                        <button type="button" class="btn btn-default edit-tool" data-open-tag="[q]" data-close-tag="[/q]">
                            &laquo;Quote&raquo;
                        </button>
                        <button type="button" class="btn btn-default edit-tool" data-open-tag='[url="http://"]' data-close-tag="[/url]">
                            <u>Link</u>
                        </button>
                    </div>
                </div>
                <textarea id="text" class="form-control" rows="10" ><?=$review->text?></textarea>
            </div>
            <div class="form-group">
                <label>Images</label>
                <div class="attach-images">
                    <?php foreach ($review->getImages() as $image) { ?>
                        <img class="image" data-full-path="<?=$image->fullPath?>" src="<?=$image->fullPath?>">
                    <?php } ?>
                </div>
                <button type="button" class="btn btn-default add-image">Add image</button>
            </div>
            <div class="form-group pull-right">
                <div class="btn-group" role="group" aria-label="...">
                    <a class="btn btn-default" href="<?=Url::toRoute(['reviews/show', 'id' => $review->id])?>" target="_blank">Preview</a>
                    <a class="btn btn-primary" id="saveButton">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>
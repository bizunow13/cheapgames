<?php
$this->registerCssFile("/css/review.css?v=1.1");
$this->title = $title;

use app\models\Reviews;
use yii\helpers\Url;
?>

<div class="page row">
    <div class="col-sm-8 post">
        <div class="container">
            <h1 class="header"><?=$title?></h1>
            <?=$text?>
        </div>
    </div>
    <div class="col-sm-4 sidebar">
        <div class="container">
            <h2 class="reviews">Обзоры</h2>
            <?php foreach (Reviews::lastReviews() as $review) { ?>
                <div class="review">
                    <div class="title">
                        <a href="<?=Url::toRoute(['reviews/show', 'id' => $review->id])?>"><?=$review->title?></a>
                    </div>
                    <div class="date">
                        <?=date("d.m.Y H:i", strtotime($review->time))?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="container">
            <h2 class="reviews">Поделиться</h2>
            <script type="text/javascript">(function() {
                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                    }})();</script>
            <div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
        </div>
    </div>
</div>

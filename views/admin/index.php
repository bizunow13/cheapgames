<?php
$this->registerJsFile('\js\jquery.min.js',  ['position' => yii\web\View::POS_END]);
$this->registerJsFile('\js\admin.js?v=1.1',  ['position' => yii\web\View::POS_END]);
$this->registerCssFile('\css\admin.css?v=1.2');
$this->title = 'Admin panel';

use yii\helpers\Url;
use app\models\Reviews; ?>

<div class="site-index">
    <div class="body-content">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
        <ul class="nav nav-tabs">
            <li class="adminSectionChoose active" data-enable="#adminSectionStatistics" role="presentation"><a href="#">Statistics</a></li>
            <li class="adminSectionChoose" data-enable="#adminSectionReviews" role="presentation"><a href="#">Reviews</a></li>
            <li class="adminSectionChoose" data-enable="#adminSectionTopNew" role="presentation"><a href="#">Top and New Games</a></li>
        </ul>
        <br>
        <div class="adminSection" id="adminSectionStatistics">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-responsive table-bordered">
                        <caption>Most popular requests</caption>
                        <thead>
                            <tr>
                                <th>Request</th>
                                <th>Count</th>
                                <!-- <th></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($requestsByMonth as $request) { ?>
                                <tr>
                                    <td width="90%"> <?=$request['text']?> </td>
                                    <td width="5%"> <?=$request['count']?> </td>
                                    <!-- <td width="5%"> <a class='requests-delete' data-request="<?=$request['text']?>" href='#'>Delete</a> </td> -->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-responsive table-bordered">
                        <caption>Most clicked requests</caption>
                        <thead>
                            <tr>
                                <th>Request</th>
                                <th>Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($clickedByMonth as $request) { ?>
                                <tr>
                                    <td width="95%"> <?=$request['text']?> </td>
                                    <td width="5%"> <?=$request['count']?> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <caption>Recent requests</caption>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Text</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($recentRequests as $request) { ?>
                                <tr>
                                    <td width="30%"> <?=$request['time']?> </td>
                                    <td width="70%"> <?=$request['text']?> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-responsive table-bordered">
                        <caption>Recent clicked requests</caption>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Game</th>
                                <th>Store</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($recentClickedByMonth as $click) { ?>
                                <tr>
                                    <td width="30%"> <?=$click['time']?> </td>
                                    <td width="60%"> <?=$click['game']?> </td>
                                    <td width="10%"> <?=$click['store']?> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <caption>Most visited stores</caption>
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($storesByMonth as $store) { ?>
                                <tr>
                                    <td width="95%"> <?=$store['name']?> </td>
                                    <td width="5%"> <?=$store['count']?> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="adminSection" id="adminSectionReviews" hidden>
            <table class="table table-bordered" id="store-table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Text</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (Reviews::find()->all() as $review) { ?>
                    <tr>
                        <td width="15%">
                            <a href="<?=Url::toRoute(['reviews/edit', 'id' => $review->id])?>">
                                <img class="img-preview" src="<?=$review->preview ? $review->preview : 'img/previews/blank.png'?>">
                            </a>
                        </td>
                        <td width="85%">
                            <div><?=$review->title?></div>
                            <div><?=$review->descr?></div>
                            <div><?=date("Y-m-d H:i", strtotime($review->time))?></div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <a class="btn btn-primary pull-right col-sm-1" href="<?=Url::toRoute('reviews/new')?>">Add</a>
        </div>
        <div class="adminSection" id="adminSectionTopNew" hidden>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Image</th>
                        <th>Game name</th>
                        <th>Request</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($newGames as $game) { ?>
                        <tr data-game-id="<?=$game->id?>">
                            <td width="10%">
                                <input type="number" class="form-control game-option" data-game-option="idx" value="<?=$game->idx?>">
                            </td>
                            <td width="6%">
                                <img class="img-game changeGameImage" src="<?=$game->img?>">
                            </td>
                            <td width="42%">
                                <input type="text" class="form-control game-option" data-game-option="name" value="<?=$game->name?>">
                            </td>
                            <td width="42%">
                                <input type="text" class="form-control game-option" data-game-option="request" value="<?=$game->request?>">
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <form id="uploadImg" hidden>
                <input type="file" id="imgFile" />
            </form>
        </div>
    </div>
</div>
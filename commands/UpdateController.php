<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Games;


class UpdateController extends Controller
{
	public $digiSellerId = 608118;
	public $updateTime;

    private function curlApiRequest($url)
    {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, '');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$value = curl_exec($ch);
		curl_close($ch);
		return $value;
    }

	private function parseZakaZaka() {
		$rawXml = $this->curlApiRequest('http://zaka-zaka.com/site/agent/?xml');
		$xml = simplexml_load_string($rawXml);

		$result = [];

		foreach ($xml->offers[0] as $offer) {
			if ((int)$offer->price > 0) {
				$result[] = [
					'updated' => $this->updateTime,
					'name' => (string)$offer->name,
					'platform' => 'Unknown',
					'store' => 'ZakaZaka',
					'price' => (int)$offer->price,
					'url' => (string)$offer->url . '/gamescheap',
				];
			}
		}

		return $result;
	}

	private function parseSteamBuy() {
		$rawXml = $this->curlApiRequest('http://steammachine.ru/api/goodslist/?v=1&format=xml');
		$xml = simplexml_load_string($rawXml);

		$result = [];

		foreach ($xml->response->data[0] as $good) {
			if ((int)$good->price->rub > 0) {
				$result[] = [
					'updated' => $this->updateTime,
					'name' => (string)$good->name,
					'platform' => 'Unknown',
					'store' => 'SteamBuy',
					'price' => (int)$good->price->rub,
					'url' => "http://steambuy.com/partner/$this->digiSellerId/$good->id_good"
				];
			}
		}

		return $result;
	}

	private function parseRoxen() {
		$rawXml = $this->curlApiRequest('http://roxen.ru/bitrix/catalog_export/yandex_885324.php');
		$xml = simplexml_load_string($rawXml);

		$result = [];

		foreach ($xml->shop->offers[0] as $offer) {
			if ((int)$offer->price > 0) {
				$result[] = [
					'updated' => $this->updateTime,
					'name' => (string)$offer->name,
					'platform' => 'Unknown',
					'store' => 'Roxen',
					'price' => (int)$offer->price,
					'url' => (string)strtok($offer->url, '?') . '?rox=1925'
				];
			}
		}

		return $result;
	}

	private function parseSteamPay() {
		$rawXml = $this->curlApiRequest('http://steampay.com/agent/608118.php');
		$xml = simplexml_load_string($rawXml);

		$result = [];

		foreach ($xml->offers[0] as $good) {
			if ((int)$good->price > 0) {
				$result[] = [
					'updated' => $this->updateTime,
					'name' => (string)$good->name,
					'platform' => 'Unknown',
					'store' => 'SteamPay',
					'price' => (int)$good->price,
					'url' => (string)$good->url
				];
			}
		}

		return $result;
	}

	private function parsePlayo() {
		$rawXml = $this->curlApiRequest('http://playo.ru/static/xmlfeed/?ref=j4k8n06a');
		$xml = simplexml_load_string($rawXml, 'SimpleXMLElement', LIBXML_NOCDATA);

		$result = [];

		foreach ($xml->playocard as $good) {
			if ((int)$good->price > 0) {
				$result[] = [
					'updated' => $this->updateTime,
					'name' => (string)$good->title,
					'platform' => 'Unknown',
					'store' => 'Playo',
					'price' => (int)$good->price,
					'url' => (string)$good->url 
				];
			}
		}

		return $result;
	}

	private function insertGames($gamesArray) {
		$columnNameArray = implode(', ', ['updated', 'name', 'platform', 'store', 'price', 'url']);

		$columnValueArray = [];
		foreach ($gamesArray as $game) {
			$columnValueArray[] = '(' . implode(', ', array_map(function ($x) {
					return Yii::$app->db->quoteValue($x);
				}, $game)) . ')';
		}
		$columnValueArray = implode(', ', $columnValueArray);

		// Возможно, здесь логичнее применить REPLACE INTO
		$sql = "INSERT INTO games ($columnNameArray) VALUES $columnValueArray ON DUPLICATE KEY UPDATE updated = '$this->updateTime'";

		$connection = Yii::$app->getDb();
		$command = $connection->createCommand($sql);
		return $command->execute();
	}

	public function actionIndex()
    {
		$this->updateTime = date("Y-m-d H:i");

		$n = $this->insertGames($this->parseZakaZaka());
		echo "Inserted $n games from ZakaZaka\n";
		$n = $this->insertGames($this->parseSteamBuy());
		echo "Inserted $n games from SteamBuy\n";
		$n = $this->insertGames($this->parseRoxen());
		echo "Inserted $n games from Roxen\n";
		$n = $this->insertGames($this->parseSteamPay());
		echo "Inserted $n games from SteamPay\n";
		$n = $this->insertGames($this->parsePlayo());
		echo "Inserted $n games from SteamPay\n";

		Games::deleteAll("updated <> '$this->updateTime'");
    }
}
<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Subscribers;
use app\models\Settings;


class DeliveryController extends Controller
{
	private $apiKey;

    private function curlApiRequest($connectorGuid, $store_url)
    {
        $url = "https://query.import.io/store/connector/" . $connectorGuid . "/_query?input=webpage/url:" . urlencode($store_url) . "&_apikey=" . urlencode($this->apiKey);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "import-io-client: import.io PHP client",
            "import-io-client-version: 2.0.0"
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        return $ch;
    }

    private function getPrice($store, $url)
    {
    	$stores = [
    		'SteamBuy'  => '5628b394-996e-4e2f-85d0-9db3d54dc002',
    		'ZakaZaka'  => 'fcb77d43-2874-4d20-899d-a7173b2d5d63',
    		'Roxen'     => '03f41832-22a4-4c90-9cf9-f744da7fc8ed',
    		'SteamPay'  => '6a863b5a-e033-4e13-9864-a247ec68c6b5',
    		'Gabestore' => 'aa6fc75a-be55-43d6-b48c-dd622fc3f0ff',
    	];

    	$ch = $this->curlApiRequest($stores[$store], $url);
    	$re = json_decode(curl_exec($ch), true);
    	curl_close($ch);

    	return isset($re['results']) ? (empty($re['results']) ? -1 : $re['results'][0]['price']) : -1;
    }

    public function actionIndex()
    {
    	$this->apiKey = Settings::getIoApiKey();

    	$checkedGames = [];

        $allSubscribers = Subscribers::find()->all();
        foreach ($allSubscribers as $subscriber) {
        	$gamesWithSale = [];
        	$trackingGames = $subscriber->trackedGames;
        	foreach ($trackingGames as $game) {
        		if (!array_key_exists($game->url, $checkedGames)) {
        			$store_price = $this->getPrice($game->store, $game->url);
        			$checkedGames[$game->url] = $store_price;
        		} else {
        			$store_price = $checkedGames[$game->url];
        		}
        		if ($store_price != -1) {
	        		if ($store_price < $game->price || $game->price == -1) {
	        			$gamesWithSale[] = $game;
	        		}
	        		if ($game->price != $store_price) {
	        			$game->updated = date('Y-m-d H:i:s');
	        			$game->price = $store_price;
	        			$game->save();
	        		}
	        	}
        	}
        	if ($gamesWithSale) {
	        	Yii::$app->mailer->compose('pricechange', ['games' => $gamesWithSale, 'unsubscribeToken' => $subscriber->remove_token])
                    ->setFrom('informer@gamescheap.ru')
					->setTo($subscriber->email)
					->setSubject("Изменение цен")
					->send();
	        }
        }
    }
}
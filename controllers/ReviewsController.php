<?php

namespace app\controllers;

use yii\helpers\Url;

use app\models\Reviews;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class ReviewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit'],
                'rules' => [
                    [
                        'actions' => ['logout', 'edit', 'new'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    /*'logout' => ['post'],*/
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*
     * Methods for work with reviews
     */

    public function actionEdit() {
        $this->layout = 'admin';
        $reviewId = isset($_GET['id']) ? $_GET['id'] : '';
        
        if ($reviewId) {
            $review = Reviews::findOne($reviewId);
            return $this->render('edit', [
                'review' => $review,
            ]);
        }

        return $this->goHome();
    }
    
    public function actionNew() {
        $new = new Reviews();
        $new->time = date('Y-m-d H:i');
        $new->public = 0;
        $new->title = 'Заголовок не указан';
        $new->descr = 'Краткое описание не указано';
        $new->text = 'Введите текст';
        $new->save();

        $this->redirect(Url::toRoute(['edit', 'id' => $new->id]));
    }

    public function actionShow() {
        $reviewId = isset($_GET['id']) ? $_GET['id'] : '';
        $review = Reviews::findOne($reviewId);

        if ($review) {
            return $this->render('show', [
                'title' => $review->title,
                'text' => $this->bb2html($review->text)
            ]);
        }
        
        return $this->goHome();
    }

    /*
     * Help
     */

    private function bb2html($text) {
        $bbcode = array("<", ">",
            "[list]", "[*]", "[/list]",
            "[img]", "[/img]",
            "[b]", "[/b]",
            "[u]", "[/u]",
            "[i]", "[/i]",
            "[s]", "[/s]",
            "[q]", "[/q]",
            "[p]", "[/p]",
            '[color="', "[/color]",
            "[size=\"", "[/size]",
            '[url="', "[/url]",
            "[mail=\"", "[/mail]",
            "[code]", "[/code]",
            '"]');
        $htmlcode = array("&lt;", "&gt;",
            "<ul>", "<li>", "</ul>",
            "<img src=\"", "\">",
            "<b>", "</b>",
            "<u>", "</u>",
            "<i>", "</i>",
            "<strike>", "</strike>",
            "<blockquote>", "</blockquote>",
            "<p>", "</p>",
            "<span style=\"color:", "</span>",
            "<span style=\"font-size:", "</span>",
            '<a href="', "</a>",
            "<a href=\"mailto:", "</a>",
            "<code>", "</code>",
            '">');
        $newtext = str_replace($bbcode, $htmlcode, $text);
        //$newtext = nl2br($newtext);
        return $newtext;
    }

    /*
     * AJAX
     */

    public function actionAjax_save_review() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $public = isset($_POST['public']) ? $_POST['public'] : null;
            $title = isset($_POST['title']) ? $_POST['title'] : null;
            $descr = isset($_POST['descr']) ? $_POST['descr'] : null;
            $text = isset($_POST['text']) ? $_POST['text'] : null;
            $images = isset($_POST['images']) ? $_POST['images'] : null;
            $result = $id && $title && $descr && $text && $images;

            if ($result) {
                $review = Reviews::findOne($id);
                $review->public = $public;
                $review->title = $title;
                $review->descr = $descr;
                $review->text = $text;
                $review->images = implode(',', $images);
                $result = $review->save();
            }

            return [
                'saved' => $result
            ];
        }
    }

    public function actionAjax_change_review_image() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $game_id = isset($_POST['game_id']) ? $_POST['game_id'] : $_POST['game_id'];
            $game_image = $_POST['image'];
            $result = $game_id;

            if ($game_id) {
                $review = Reviews::findOne($game_id);
                $review->preview = $game_image;
                $result = $review->save();
            }

            return [
                'result' => $result,
                /*'error'  => $review->getErrors()*/
            ];
        }
    }

}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\ContactForm;

use app\models\Subscribers;
use app\models\TrackingGames;


class SubscribeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*
     * For eMail-delivery
     */

    public function actionUnsubscribe()
    {
        $removeToken = isset($_GET['token']) ? $_GET['token'] : null;
        $subscriber = Subscribers::findByRemoveToken($removeToken);
        if ($subscriber) {
            foreach ($subscriber->trackedGames as $game)
                $game->delete();
            $subscriber->delete();
            Yii::$app->session->setFlash('unSubscribe');
        }
        return $this->goHome();
    }

    public function actionDelete_game()
    {
        $removeToken = isset($_GET['token']) ? $_GET['token'] : null;
        $trackingGame = TrackingGames::findByRemoveToken($removeToken);
        if ($trackingGame) {
            $subscriber = $trackingGame->relatedSubscriber;
            $trackingGame->delete();
            if (!$subscriber->trackedGames)
                $subscriber->delete();
            Yii::$app->session->setFlash('deleteGame');
        }
        return $this->goHome();
    }

    public function actionRedirect_to_game()
    {
        $redirectToken = isset($_GET['token']) ? $_GET['token'] : null;
        $trackingGame = TrackingGames::findByRedirectToken($redirectToken);
        if ($trackingGame) {
            $this->redirect($trackingGame->url);
        }
    }

    /*
     * AJAX
     */

    public function actionAjax_subscribe()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $email = isset($_POST['email']) ? $_POST['email'] : null;
            $email = filter_var($email, FILTER_VALIDATE_EMAIL) ? $email : null;
            $games = isset($_POST['games']) ? json_decode($_POST['games']) : null;
            $result = $email && $games;

            if ($result) {
                $user = Subscribers::findByEmail($email);
                $user = $user === null ? Subscribers::add($email) : $user;
                $result = (bool)$user;

                if ($result) {
                    foreach ($games as $game) {
                        if (!TrackingGames::isTracked($user->id, $game->name, $game->store)) {
                            $result = TrackingGames::add(
                                $user->id,
                                $game->store,
                                $game->partnerUrl ? $game->partnerUrl : $game->url,
                                $game->name,
                                $game->price
                            );
                            if (!$result) {
                                break;
                            }
                        }
                    }
                }
            }
            return [
                'subscribed' => $result
            ];
        }
    }
}

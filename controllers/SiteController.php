<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Games;
use app\models\History;
use app\models\Follows;
use app\models\NewGames;
use app\models\Reviews;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    private function groupByGame($list)
    {
        $games = [];

        foreach ($list as $store => $data) {
            foreach ($data as $game) {
                $games[$game['name']][] = array_merge(['store' => $store], $game);
            }
        }

        $mergedGames = [];
        foreach ($games as $gameKey1 => $gameData1) {
            $unifiedGameKey1 = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $gameKey1), 'UTF-8');
            foreach ($games as $gameKey2 => $gameData2) {
                $unifiedGameKey2 = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $gameKey2), 'UTF-8');
                if ($unifiedGameKey1 == $unifiedGameKey2) {
                    if ($gameKey1 > $gameKey2) {
                        $mergedGames[$gameKey1] = array_merge($gameData1, $gameData2);
                        unset($games[$gameKey1]);
                        unset($games[$gameKey2]);
                    }
                }
            }
        }

        $games = array_merge($games, $mergedGames);

        $withManyStores = [];
        $withOneStore = [];

        foreach ($games as $game => $stores) {
            if (count($stores) > 1) {
                $withManyStores[$game] = $stores;
            } else {
                $withOneStore[] = array_merge($stores[0], ['name' => $game]);
            }
        }

        return array_merge($withManyStores, ['Unique' => $withOneStore]);
    }

    private function groupByPlatform($list)
    {
        $games = [];

        foreach ($list as $store => $data) {
            foreach ($data as $game) {
                $games[$game['platform']][] = array_merge(['store' => $store], $game);
            }
        }

        $mergedGames = [];
        foreach ($games as $gameKey1 => $gameData1) {
            $unifiedGameKey1 = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $gameKey1), 'UTF-8');
            foreach ($games as $gameKey2 => $gameData2) {
                $unifiedGameKey2 = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $gameKey2), 'UTF-8');
                if ($unifiedGameKey1 == $unifiedGameKey2) {
                    if ($gameKey1 > $gameKey2) {
                        $mergedGames[$gameKey1] = array_merge($gameData1, $gameData2);
                        unset($games[$gameKey1]);
                        unset($games[$gameKey2]);
                    }
                }
            }
        }

        $games = array_merge($games, $mergedGames);

        return $games;
    }

    private function sortGamesByPrice($result)
    {
        foreach ($result as $key => $data) {
            usort($data, function ($a, $b) {
                return $b['price'] - $a['price'];
            });
            $result[$key] = $data;
        }

        return $result;
    }

    private function limitByStore($results, $limit)
    {
        foreach ($results as $key => $data) {
            if (count($data)) {
                $results[$key] = array_slice($data, 0, $limit);
            }
        }
        return $results;
    }

    private function declOfNum($number, $titles)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
    }

    private function resultsCount($results) {
        $count = 0;
        foreach ($results as $key => $data) {
            $count += count($data);
        }
        return [
            'total' => 'Всего ' . $this->declOfNum($count, ['найдена: ', 'найдено: ', 'найдено: ']),
            'count' => $count . $this->declOfNum($count, [' игра', ' игры', ' игр'])
        ];
    }

    public function actionIndex()
    {
        $searchRequest = isset($_GET['game']) && strlen($_GET['game']) > 2 ? $_GET['game'] : '';

        $example = '';
        $nothingFound = null;
        $results = [];
        $resultsByPlatform = [];
        $resultsByGame = [];

        if ($searchRequest) {
            History::saveRequest($searchRequest);
            $results = Games::search($searchRequest);
            /*$results = $this->prepareImportIoData($results);*/
            $nothingFound = empty($results);

            if ($results) {
                $results = $this->sortGamesByPrice($results);
                $results = $this->limitByStore($results, 10);
                $resultsByGame = $this->groupByGame($results);
                $resultsByPlatform = $this->groupByPlatform($results);
            }
        } else {
            $searchExamples = History::getMostPopularRequests();
            if ($searchExamples) {
                $example = $searchExamples[rand(0, max(count($searchExamples) - 1, 0))]['text'];
            }
        }

        return $this->render('index', [
            'request' => $searchRequest,
            'example' => $example,
            'nothingFound' => $nothingFound,
            'results' => $results,
            'resultsByGame' => $resultsByGame,
            'resultsByPlatform' => $resultsByPlatform,
            'resultsCount' => $this->resultsCount($results),
            //'groupBy' => $groupBy,
            //'stores' => Stores::find()->all(),
            'topRequests' => History::getMostPopularRequests(),
            'newGames' => NewGames::sorted(),
            'lastReviews' => Reviews::lastReviews()
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect(Yii::$app->urlManager->createUrl("admin/index"));
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /*
     * AJAX
     */

    public function actionAjax_save_follow()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            $text = isset($_POST['text']) ? $_POST['text'] : null;
            $game = isset($_POST['game']) ? $_POST['game'] : null;
            $platform = isset($_POST['platform']) ? $_POST['platform'] : null;
            $store = isset($_POST['store']) ? $_POST['store'] : null;
            $price = isset($_POST['price']) && is_numeric($_POST['price']) ? intval($_POST['price']) : null;
            $result = $text && $game && $platform && $store && $price;

            if ($result) {
                $result = Follows::add($text, $game, $platform, $store, intval($price));
            }

            return [
                'saved' => $result
            ];
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Stores;
use app\models\Settings;
use app\models\History;
use app\models\Follows;
use app\models\NewGames;
use app\models\Reviews;

class AdminController extends Controller
{
    public $layout = 'admin.php';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'requestsByMonth' => History::getMostPopularRequests(),
            'recentRequests' => History::getRecentRequests(),
            'storesByMonth' => Follows::mostVisitedStores(),
            'clickedByMonth' => Follows::getMostClickedStores(),
            'recentClickedByMonth' => Follows::recentClickedRequestsByPeriod(),
            'newGames' => NewGames::sorted(),
            'lastReviews' => Reviews::lastReviews()
        ]);
    }

    /*
     * AJAX
     */

    /*public function actionAjax_save_settings_option()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $option_id = isset($_POST['option_id']) && is_numeric($_POST['option_id']) ? $_POST['option_id'] : null;
            $option_value = isset($_POST['option_value']) ? $_POST['option_value'] : null;
            $result = $option_id && $option_value;

            if ($result) {
                $option = Settings::findOne($option_id);
                $option->value = $option_value;
                $result = $option->save();
            }

            return [
                'saved' => $result
            ];
        }
    }

    public function actionAjax_store_option_change()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $store_id = isset($_POST['store_id']) && is_numeric($_POST['store_id']) ? $_POST['store_id'] : null;
            $store_option = isset($_POST['store_option']) ? $_POST['store_option'] : null;
            $store_option = $store_option ? (in_array($store_option, ['name', 'io_connector', 'partner_key']) ? $store_option : null) : null;
            $value = isset($_POST['value']);
            $result = $store_id && $store_option && $value;

            if ($result) {
                $store = Stores::findOne($store_id);
                $store->$store_option = $_POST['value'] ? $_POST['value'] : null;
                $result = $store->save();
            }

            return [
                'saved' => $result
            ];
        }
    }*/

    public function actionAjax_save_new_store()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $store_name = isset($_POST['store_name']) ? $_POST['store_name'] : null;
            $store_connector = isset($_POST['store_connector']) ? $_POST['store_connector'] : null;
            $store_partner_key = isset($_POST['store_partner_key']) ? $_POST['store_partner_key'] : null;
            $result = $store_name && $store_connector && $store_partner_key;

            if ($result) {
                $store = new Stores();
                $store->name = $store_name;
                $store->io_connector = $store_connector;
                $store->partner_key = $store_partner_key;
                $result = $store->save();
            }

            return [
                'saved' => $result,
                'new_store_id' => $store->id
            ];
        }
    }

    public function actionAjax_store_delete()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            $store_id = isset($_POST['store_id']) && is_numeric($_POST['store_id']) ? $_POST['store_id'] : null;
            $result = $store_id;

            if ($result) {
                $store = Stores::findOne($store_id);
                $result = $store->delete();
            }

            return [
                'deleted' => $result
            ];
        }
    }

    public function actionAjax_change_image()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $game_id = isset($_POST['game_id']) ? $_POST['game_id'] : $_POST['game_id'];
            $game_image = $_POST['image'];
            $result = $game_id;

            if ($game_id) {
                $game = NewGames::findOne($game_id);
                $game->img = $game_image;
                /// НАДО СОХРАНЯТЬ ТОЛЬКО ИМЯ ИЗОБРАЖЕНИЯ!!!!!!!!!!!!!!!
                // СДЕЛАТЬ ЗАГРУЗКУ ИЗОБРАЖЕНИЙ В РЕВЬЮВС!!!!!!!!!!!!!!!!!!
                $result = $game->save();
            }

            return [
                'result' => $result,
                'error'  => $game->getErrors()
            ];
        }
    }

    public function actionAjax_game_option_change()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $game_id = isset($_POST['game_id']) && is_numeric($_POST['game_id']) ? $_POST['game_id'] : null;
            $game_option = isset($_POST['game_option']) ? $_POST['game_option'] : null;
            $game_option = $game_option ? (in_array($game_option, ['idx', 'name', 'request']) ? $game_option : null) : null;
            $value = isset($_POST['value']) ? $_POST['value'] : null;
            $result = $game_id && $game_option && $value;
            
            if ($result) {
                $game = NewGames::findOne($game_id);
                $game->$game_option = $value;
                $result = $game->save();
            }

            return [
                'saved' => $result,
            ];
        }
    }
}

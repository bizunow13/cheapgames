<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class UploadController extends Controller
{
    /*public $layout = 'admin.php';*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->goHome();
    }

    /*
     * AJAX
     */

    public function actionAjax_upload_file()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $image_type = isset($_POST['type']) ? $_POST['type'] : null;
            $upload_result = $_FILES['file']['error'] == UPLOAD_ERR_OK;
            $result = $image_type && $upload_result;
            $error = $result ? 'No error' : 'File upload error';

            $path = null;
            $name = null;

            if ($result) {
                $tmp_name = $_FILES['file']['tmp_name'];
                $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $name = Yii::$app->security->generateRandomString() . ".$extension";
                
                // КАРТИНКА ЛИ ЭТО?
                // Нужно пееределать свитч. Вынести проверки размеров, типов и присвоение путей. Оптимизировать код. 
                
                switch ($image_type) {
                    case 'post-image': {
                        list($width, $height) = getimagesize($tmp_name);
                        $check_size = $width == 694;

                        if($check_size) {
                            $result = true;
                            $path = "img/review-images/$name";
                            move_uploaded_file($tmp_name, $path);
                        } else {
                            $result = false;
                            $error = "Size error $width x $height";
                        }
                    } break;
                    case 'game-logo': {
                        list($width, $height) = getimagesize($tmp_name);
                        $check_size = $width == 230 && $height == 108;

                        if($check_size) {
                            $result = true;
                            $path = "img/game-logos/$name";
                            move_uploaded_file($tmp_name, $path);
                        } else {
                            $result = false;
                            $error = "Size error $width x $height";
                        }
                    } break;
                    default: {
                        $result = false;
                        $error = 'Unknown image type';
                    }
                }
            }

            return [
                'result' => $result,
                'error' => $error,
                'file' => [
                    'path' => $path,
                    'name' => $name
                ]
            ];
        }
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property integer $id
 * @property string $time
 * @property string $text
 * @property string $game
 * @property string $platform
 * @property string $store
 * @property integer $price
 */
class Follows extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'follows';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'text', 'game', 'platform', 'store', 'price'], 'required'],
            [['time'], 'safe'],
            [['price'], 'integer'],
            [['text', 'game', 'platform'], 'string', 'max' => 45],
            [['store'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'text' => 'Text',
            'game' => 'Game',
            'platform' => 'Platform',
            'store' => 'Store',
            'price' => 'Price',
        ];
    }

    public static function add($text, $game, $platform, $store, $price)
    {
        $time = new \DateTime();
        $time = $time->format('Y-m-d H:i:s');

        $follow = new Follows();
        $follow->time = $time;
        $follow->text = $text;
        $follow->game = $game;
        $follow->platform = $platform;
        $follow->store = $store;
        $follow->price = $price;
        return $follow->save();
    }
    
    public static function mostVisitedStores()
    {
        $to = date("Y-m-d");
        $from = date("Y-m-d", strtotime("-1 month", strtotime(date($to))));

        return Follows::find()
            ->select(['store AS name', 'COUNT(store) AS count'])
            ->where(['between','time', $from, $to])
            ->groupBy(['store'])
            ->orderBy([new \yii\db\Expression("count DESC")])
            ->limit(10)
            ->asArray()
            ->all();
    }

    public static function getMostClickedStores()
    {
        $to = date("Y-m-d");
        $from = date("Y-m-d", strtotime("-1 month", strtotime(date($to))));

        return Follows::find()
            ->select(['text', 'COUNT(text) AS count'])
            ->where(['between','time', $from, $to])
            ->groupBy(['text'])
            ->orderBy([new \yii\db\Expression("count DESC")])
            ->limit(10)
            ->asArray()
            ->all();
    }

    public static function recentClickedRequestsByPeriod()
    {
        return Follows::find()
            ->select(['time', 'game', 'store'])
            ->orderBy([new \yii\db\Expression("time DESC")])
            ->limit(10)
            ->asArray()
            ->all();
    }
}

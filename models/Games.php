<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property string $updated
 * @property string $name
 * @property string $platform
 * @property string $store
 * @property integer $price
 * @property string $url
 */
class Games extends \yii\db\ActiveRecord
{

    const GROUP_RESULTS_BY_STORE = 0;
    const GROUP_RESULTS_BY_GAME = 1;
    const GROUP_RESULTS_BY_PLATFORM = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated', 'name', 'platform', 'store', 'price', 'url'], 'required'],
            [['updated'], 'safe'],
            [['name', 'url'], 'string'],
            [['price'], 'integer'],
            [['platform'], 'string', 'max' => 45],
            [['store'], 'string', 'max' => 90],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'updated' => 'Updated',
            'name' => 'Name',
            'platform' => 'Platform',
            'store' => 'Store',
            'price' => 'Price',
            'url' => 'Url',
        ];
    }
    
    public static function count() {
        return Games::find()
            ->select('count(*)')
            ->scalar();
    }
    
    private function simplyString($string) {
        return mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $string), 'UTF-8');
    }

    public static function search($text) {
        $sql = 'SELECT * FROM `games` WHERE MATCH (name) AGAINST (' . Yii::$app->db->quoteValue($text) . ');';

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $searchResults = $command->queryAll();
        
        $result = [];
        foreach ($searchResults as $row) {
            $result[$row['store']][] = $row;
        }

        /*if ($group == self::GROUP_RESULTS_BY_STORE) {
            foreach ($searchResults as $row) {
                $result[$row->store] = $row;
            }
        }*elseif ($group == self::GROUP_RESULTS_BY_GAME) {
            foreach ($searchResults as $row) {
                if (self::simplyString($row->name))
                $result[$row->name] = $row;
            }
        }*/
        
        return $result;
    }
}

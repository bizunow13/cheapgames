<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "new_games".
 *
 * @property integer $id
 * @property integer $idx
 * @property string $img
 * @property string $name
 * @property string $request
 */
class NewGames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'new_games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idx', 'img', 'name', 'request'], 'required'],
            [['idx'], 'integer'],
            [['img', 'name', 'request'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idx' => 'Idx',
            'img' => 'Img',
            'name' => 'Name',
            'request' => 'Request',
        ];
    }

    public static function sorted()
    {
        return NewGames::find()->orderBy('idx')->all();
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscribers".
 *
 * @property integer $id
 * @property string $time
 * @property string $email
 * @property string $remove_token
 */
class Subscribers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'email', 'remove_token'], 'required'],
            [['time'], 'safe'],
            [['email'], 'string', 'max' => 254],
            [['remove_token'], 'string', 'max' => 32],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'email' => 'Email',
            'remove_token' => 'Remove Token',
        ];
    }

    public function getTrackedGames()
    {
        return $this->hasMany(TrackingGames::className(), ['subscriber' => 'id']);
    }

    public static function findByEmail($email)
    {
        return Subscribers::findOne(['email' => $email]);
    }

    public static function findByRemoveToken($token)
    {
        return Subscribers::findOne(['remove_token' => $token]);
    }

    public static function add($email)
    {
        $time = new \DateTime();
        $time = $time->format('Y-m-d H:i:s');

        $user = new Subscribers();
        $user->time  = $time;
        $user->email = $email;
        $user->remove_token = Yii::$app->security->generateRandomString();
        $user->save();

        return $user;
    }
}

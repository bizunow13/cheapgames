<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string $time
 * @property string $text
 */
class History extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'text'], 'required'],
            [['time'], 'safe'],
            [['text'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'text' => 'Text',
        ];
    }

    public static function saveRequest($text)
    {
        $time = new \DateTime();
        $time = $time->format('Y-m-d H:i:s');

        $requestToSave = new History();
        $requestToSave->text = $text;
        $requestToSave->time = $time;
        return $requestToSave->save();
    }

    public static function getMostPopularRequests()
    {
        $to = date("Y-m-d");
        $from = date("Y-m-d", strtotime("-1 month", strtotime(date($to))));

        return History::find()
            ->select(['text', 'COUNT(text) AS count'])
            ->where(['between','time', $from, $to])
            ->groupBy(['text'])
            ->orderBy([new \yii\db\Expression("count DESC")])
            ->limit(10)
            ->asArray()
            ->all();
    }

    public static function getRecentRequests()
    {
        return History::find()
            ->select(['text', 'time'])
            ->orderBy([new \yii\db\Expression("time DESC")])
            ->limit(10)
            ->asArray()
            ->all();
    }
}

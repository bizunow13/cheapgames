<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property string $time
 * @property integer $public
 * @property string $preview
 * @property string $title
 * @property string $descr
 * @property string $text
 * @property string $images
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'public', 'title', 'descr', 'text'], 'required'],
            [['time'], 'safe'],
            [['public'], 'integer'],
            [['text', 'images'], 'string'],
            [['preview'], 'string', 'max' => 90],
            [['title'], 'string', 'max' => 45],
            [['descr'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'public' => 'Public',
            'preview' => 'Preview',
            'title' => 'Title',
            'descr' => 'Descr',
            'text' => 'Text',
            'images' => 'Images',
        ];
    }

    public static function lastReviews() {
        return Reviews::find()
            ->where(['public' => '1'])
            ->orderBy([new \yii\db\Expression("time DESC")])
            ->limit(10)
            ->all();
    }

    public function getImages() {
        $list = explode(',', $this->images);
        $resultList = [];

        foreach ($list as $index => $image) {
            if ($image) {
                $resultList[] = (object)[
                    'index' => $index,
                    /*'fileName' => $image,*/
                    'fullPath' => $image
                ];
            }
        }

        return $resultList;
    }
}

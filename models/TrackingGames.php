<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracking_games".
 *
 * @property integer $id
 * @property integer $subscriber
 * @property string $updated
 * @property string $store
 * @property string $url
 * @property string $name
 * @property integer $price
 * @property string $remove_token
 * @property string $redirect_token
 */
class TrackingGames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking_games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriber', 'updated', 'store', 'url', 'name', 'price', 'remove_token', 'redirect_token'], 'required'],
            [['subscriber', 'price'], 'integer'],
            [['updated'], 'safe'],
            [['store'], 'string', 'max' => 45],
            [['url', 'name'], 'string', 'max' => 90],
            [['remove_token', 'redirect_token'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscriber' => 'Subscriber',
            'updated' => 'Updated',
            'store' => 'Store',
            'url' => 'Url',
            'name' => 'Name',
            'price' => 'Price',
            'remove_token' => 'Remove Token',
            'redirect_token' => 'Redirect Token',
        ];
    }

    public function getRelatedSubscriber()
    {
        return $this->hasOne(Subscribers::className(), ['id' => 'subscriber']);
    }

    public static function findByRemoveToken($token)
    {
        return TrackingGames::findOne(['remove_token' => $token]);
    }

    public static function findByRedirectToken($token)
    {
        return TrackingGames::findOne(['redirect_token' => $token]);
    }

    public static function isTracked($id, $name, $store)
    {
        return TrackingGames::findOne([
            'subscriber' => $id, 
            'name' => $name, 
            'store' => $store
        ]); 
    }

    public static function add($userId, $store, $url, $gameName, $price)
    {
        $time = new \DateTime();
        $time = $time->format('Y-m-d H:i:s');

        $game = new TrackingGames();
        $game->subscriber = $userId;
        $game->updated = $time;
        $game->store = $store;
        $game->url = $url;
        $game->name = $gameName;
        $game->price = $price;
        $game->remove_token = Yii::$app->security->generateRandomString();
        $game->redirect_token = Yii::$app->security->generateRandomString();
        return $game->save();
    }
}
